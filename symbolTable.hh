#ifndef SYMBOLTABLE_HH
#define SYMBOLTABLE_HH

#include <iostream>
#include <vector>
#include <cassert>
#include <unordered_map>
#include <iterator>
#include <climits>
#include <sstream>
#include <stdexcept>
#include <memory>
#include <typeinfo>
#include <string>
#include "defs.h"

using namespace std;


namespace stab {
enum types {
   NO_TYPE = 0, INT_TYPE, UNSIGNED_TYPE, CHAR_TYPE
};

enum regs {
   RO=0, R1, R2, R3, R4, R5, R6, R7, R8, R9, R10, R11, R12, R13, R14, R15
};

enum scopes{
   GLOBAL_SCOPE=0,LOCAL_SCOPE=1

};
ostream& operator <<(ostream& os, types rhs);

}






class Attribute {
public:
   virtual int getAttribute() const {
      assert(0);
      return 42;
   }
   virtual void addParamType(stab::types type) {
      assert(0);
      cout << type;
   }
   virtual stab::types getParamType(unsigned paramNum) const {
      assert(0);
      cout << paramNum;
   }
   virtual unsigned numOfArgs() const {
      assert(0);
      return 42;
   }

   virtual void setAttribute(int newAttr) {
      assert(0);
      cout << newAttr;
   }
   
   
   
   virtual ~Attribute() {
   }
   virtual Attribute& operator=(const Attribute& at) {
      assert(0);
   }

};

ostream& operator<<(ostream& os, const Attribute& attr);


class globalAttribute: public Attribute {
   vector<stab::types> *ptr_;
public:
   globalAttribute(bool isFunction) {
      if (isFunction) {
         ptr_ = new vector<stab::types>;
      } else {
         ptr_ = nullptr;
      }
   }
   /**
    * getParamType Vraca tip odgovarajuceg parametra funkcije
    * ako globalni atribut jeste funkcija, a ako nije
    * [to se moze desiti u slucaju da korisnik pokusa
    * da pozove globalnu variablu] vraca NO_TYPE. Prijava
    * greske se osavlja parseru. NO_TYPE vraca i kada pokusa
    * da se indeksira van opsega[ poziva se funkcija sa 
    * vise argumenata nego sto ima parametara].
    * @param  :indeks vektora parametra
    * @return :tip indeksiranog parametra
    */
    
   stab::types getParamType(unsigned paramNum) const {
      assert(ptr_ != nullptr);
      if(ptr_==nullptr || paramNum >= ptr_->size()){
         return stab::NO_TYPE;
      }
         return ptr_->at(paramNum);
   }
   /**
    * Ova metoda se zove iskljucivo kod parsiranja definicije funkcije
    * ptr ne moze biti nullptr posto ovo koristimo kad smo vec isparsirali
    * ime funkcije
    * @param : tip parametra funkcije koju trenutno parsiramo
    */
   void addParamType(stab::types type) {
      assert(ptr_ != nullptr);
      ptr_->push_back(type);
   }
   /**
    * @brief : vraca broj parametara funkcije (uglavnom kad je zovemo)
    * @return : 0 ako za nepostojecu funkciju ili globalnu variablu
    * broj parametara za postojecu
    */
    
   unsigned numOfArgs() const {
      if(ptr_==nullptr){
         return 0;
      }
      return ptr_->size();
   }
   /**
    * @brief : destruktor, oslobadja ono sto default ctor zauzeo
    */
   ~globalAttribute() {
      delete (ptr_);
   }
   void setAttribute(int newAttr) {
      assert(0);
      cout << newAttr;
   }
   /**
    * @return :0 ako je globalna variable 1 ako je funkcija
    */

   int getAttribute() const {
      if (ptr_ == nullptr)
         return 0;
      else
         return 1;
   }

};

class variableAttribute: public Attribute {
   int attribute_;
public:
   /**
    * @brief :default konstruktor, postavlja atribut na nula
    * nikad ne bi trebalo da ostane na ovome
    */
   variableAttribute() :
         attribute_(0) {
   }
   /**
    * @brief : nekad je jednostavnije odmah da postavimo atribut
    */
   variableAttribute(int attribute) :
         attribute_(attribute) {
   }
   /**
    * @brief : dobavlja atribut za lok variablu
    * @return : negativni broj ako je u pitanju parametar + u suprotnom
    */
   int getAttribute() const {
      return attribute_;
   }
   /**
    * @brief : setter
    */
   void setAttribute(int newAttr) {
      attribute_ = newAttr;
   }
   void addParamType(stab::types type) {
      assert(0);
      cout << type;
   }
   stab::types getParamType(unsigned paramNum) const {
      assert(0);
      cout << paramNum;
      return stab::NO_TYPE;
   }
   unsigned numOfArgs() const {
      assert(0);
   }
};
/**
 * Struktura vrednosti simbola u SymbolTable. Staticke velicine
 * attribute_ptr ili pokazuje na vector ili na obican int
 * default ctor se ne koristi u praksi, samo za testiranje
 * u dtor-u mozda nepotrebno radimo reset smart ptr-a. Verovatno
 * bi se to automatski uradili. Tako mu dajemo do znanja da jedan
 * manje ptr pokazuje na taj deo memorije
 */

struct symbolValue {
   string name;
   stab::types type;
   shared_ptr<Attribute> attribute_ptr;
   symbolValue() :
         name { "ime" }, type { stab::NO_TYPE }, attribute_ptr{new globalAttribute{false} }{

   }
   symbolValue(string name, stab::types type, Attribute* attribute) :
         name(name), type(type), attribute_ptr { attribute } {
   }
   
   ~symbolValue() {
      attribute_ptr.reset();
   }

};
/**
 * @brief : << operator za symbolValue
 * @return : << ispis u obliku name: std::string
 *                             type: stab::types
 *                             attribute: <vector> | <int>
 */

ostream& operator<<(ostream& os, const symbolValue& symVal);

/**
 * Tabela jednog opsega, unordered mapa je privatni clan, u nju smestamo
 * sve variable iz tog opsega. 
 * insertSymbol:
 *    @return : true ako se simbol vec nalazi u tabeli, false otherwise
 *    @param : vec kreiran symbolValue, ovaj ga samo ubacuje
 * find:
 *    @return : iterator na element unordered_mape ako je nadjen, end() inace
 *    @param : ime (kljuc) elementa kog trazimo
 */

class ScopeTable {
private:
   unordered_map<string, symbolValue> tableMap;
public:

   bool insertSymbol(const symbolValue& symVal) {
      return tableMap.insert(pair<string,symbolValue>(symVal.name,symVal)).second;
   }
   auto find(string name)->decltype(tableMap.find(name)) {
      return tableMap.find(name);
   }
   size_t size() {
      return tableMap.size();
   }

   auto end()->decltype(tableMap.end()) {
      return tableMap.end();
   }

   friend ostream& operator<<(ostream& out, const ScopeTable& sTable);
};

/**
 *@brief : ispisivanje elemenata scopeTable preko iteratora hash mape
 */

ostream& operator<<(ostream& out, const ScopeTable& sTable);

/**
 * @class : SymbolTable
 * privatna polja:
 */


class SymbolTable {

private:
   size_t free_reg_index; ///< indeksira sledeci slobodan registar
                          ///< tj. element vektora registara
                          
   vector<ScopeTable> symtab; ///< vektor hash mapa. po jedna za
                              ///< za svaki scope i jedna globalna
                              ///< koja se kreira pri konstrukciji
                              
   unordered_map<string, stab::types> constantTable; ///< tabela konstanti
                                                     ///< sadrzi samo 
                                                     ///< konstante u opsegu
   
   vector<stab::types> registers;///< vektor registara, na pocetku svi 
                                 ///< slobodni tj stab::NO_TYPE
                     
public:

   static const size_t NUM_OF_REGISTERS = 16;///< ukupan broj registara
   
   //! member function tryToInsertID
   /**
    * @brief ubacuje se vrednost u tabelu simbola
    * @param ime (kljuc) symbolValue-a
    * @param tip simbola
    * @param atribut, ili pokazivac na vektor ili na int
    * @return true ako je ubacen u tabelu simbola false, ako vec postoji\
    * vrednost sa tim kljucem
    */

   bool tryToInsertID(string name, stab::types type, Attribute* attribute) {
      assert(!symtab.empty());
      symbolValue symVal { name, type, attribute };
      return symtab.back().insertSymbol(symVal);
   }
   //! funkcija clanica getType
   /** 
    * @brief : vraca tip cega god joj se prosledi
    * @param : pokazivac na uglavnom kljuc tj ime simbola/funkcije
    * @param : default kind, samo optimizacija
    * @return : tip simbola ciji je kljuc strin na koji ovaj pokazuje
    * 
    */
   
   stab::types getType(std::string *stringPtr,kinds kind=VAR){
      assert(stringPtr!=nullptr);
      if( isConstant(*stringPtr) ){
         return getConstantType (*stringPtr);
      }else if( isRegister(*stringPtr) ){
         return getRegisterType(stoul( (*stringPtr).substr(1) ) );
      }else{
         return getVarFuncType (*stringPtr,kind);
      }
   }
   //! funkcija clanica isRegister
   /**
    * @param : string koji je mozda registar
    * @return : true ako jeste string reprezentacija registra
    */
   bool isRegister(string& maybeRegister) const{
      return maybeRegister[0]=='%'? true : false ;
   }
   //! funkcija clanica getRegisterType
   /**
    * @return :ako je zauzet registar onda vraca njegov tipa, ako nije
    * vraca no type. 
    * @todo : Mozda treba ovo negde da iskoristimo da ne treba da vraca
    * no type
    * @param : index vektora registara
    * pada na assertu ako pokusamo van opsega da 
    * indeksiramo
    * @throw : out of range. ali mislim da nikada necemo doci do toga
    */
   
   stab::types getRegisterType(size_t index){
      assert( index<=NUM_OF_REGISTERS-1);
      return registers.at(index);
   }
   //! funkcija clanica za nalazenje tipa konstante
   /**
    * @param : string, kljuc konstante
    * @return : tip konstante, mora da bude nadjen
    * assert ako nema konstante. ne sme doci do ovoga
    * i nece, posto ce je 100% prepoznati kao konstantu 
    * ako moze da bude konstanta
    */
   
   stab::types getConstantType(const string& constantName){
      auto it=constantTable.find(constantName);
      if (it!=constantTable.end()){
         cout<<"tip je: "<<it->second<<endl;
         return it->second;
      } else{
         assert(0);
         return stab::NO_TYPE;
      }
   }
   
   //! funkcija clanica getVarFuncType vraca tip funkcije ili variable
   /**
    * @return : tip funkcije i variable ako jeste definisana, NO_TYPE ako \
    * nije
    * @param : ime funkcije/variable (kljuc tabele) i optimizacija kind
    * @todo : mozda treba da pazimo da proverimo da li je stab::NO_TYPE
    */
   
   stab::types getVarFuncType(const string& varFuncName, unsigned kind=VAR){
      auto it=lookupID(varFuncName,kind);
      if(it!=end()){
         cout<<"tip je: "<<it->second.type<<endl;
         return it->second.type;
      } else
         return stab::NO_TYPE;
   }
   
   size_t takeReg(){
      if(free_reg_index>stab::R12){
         cerr<<"Compiler error! No free registers!"<<endl;
         exit(EXIT_FAILURE);
      }
      cout<<"zauzimamo registar "<<free_reg_index<<endl;
      return free_reg_index++;
   }
   
   void freeReg(){
         if(free_reg_index<1){
            cerr<<"Compiler error! No more registers to free!"<<endl;
            exit(EXIT_FAILURE);
         }
         setRegisterType(free_reg_index--,stab::NO_TYPE);
   }
   
   

   void tryToInsertIntConstant(string constant) {
         int constantValue;

         try {
            constantValue = stoi(constant);
         } catch (out_of_range& err) {
            cerr << "error: Constant " << constant << " out of range. " << endl;
         } catch (invalid_argument& ia) {
            assert(0);
         }
         cout << "OK, ispravan int: " << constantValue << endl;
          constantTable.insert(make_pair(constant, stab::INT_TYPE));
   }
   
   void tryToInsertUnsignedConstant(string constant){
      
         unsigned constantValue;
         if (constant.front() == '-') {
            cerr << "warning: Greska programera. Negativan broj" << endl;
         }
         try {
            constantValue = stoul(constant);
         } catch (out_of_range& err) {
            cerr << "error: Constant " << constant << " out of range. " << endl;
         } catch (invalid_argument& ia) {
            assert(0);
         }
         cout << "OK: " << constantValue << endl;
         
         constantTable.insert(make_pair(constant, stab::UNSIGNED_TYPE));
   }
   
   void tryToInsertCharConstant(string charConstant){
         constantTable.insert(make_pair(charConstant,stab::CHAR_TYPE));
      
   }
   
   bool isConstant(string maybeConstant) const {
      return (constantTable.find(maybeConstant)!=constantTable.end())? true : false;
   }
   

   auto lookupID(string name,
         unsigned kind = VAR)->decltype(symtab.begin()->end()) {

      assert(kind == VAR || kind == FUNCTION);

      auto rbeginIterator = (kind == VAR) ? symtab.rbegin() : symtab.rend() - 1;

      for (auto it = rbeginIterator; it != symtab.rend(); ++it) {
         if (((*it).find(name)) != (*it).end()) {
            return ((*it).find(name));
         }
      }
      return end();
   }

   auto end()->decltype(symtab.begin()->end()) {
      return symtab.begin()->end();
   }

   void newScope() {
      symtab.push_back(ScopeTable { });
      cout<<"Ulazim u  opseg..."<<symtab.size()<<endl;
   }
   unsigned getScope(){
      cout<<"trenutni opseg: "<<symtab.size()<<endl;
      return (symtab.size()==1)?stab::GLOBAL_SCOPE : stab::LOCAL_SCOPE;
   }
   

   void setRegisterType(int registerIndex, stab::types type) {
      assert(registerIndex >= 0 && registerIndex <= 13);
      cout<< "stavljamo registar "<<registerIndex<<" na tip "<<type<<endl;
      registers.at(registerIndex) = type;
   }
   
   void setRegisterType(const string& registerName, stab::types type){
      registers.at(stoul(registerName.substr(1)))=type;
      cout<< "stavljamo registar "<<registerName<<" na tip "<<type<<endl;
     // printSymtab();
   }

   void clearScope() {
      cout<<"Izlazim iz opsega "<<symtab.size()<<endl;
      symtab.pop_back();
   }

   void printSymtab() {
      for (auto it = constantTable.begin(); it != constantTable.end(); ++it) {
         cout << "Constant Table: " << endl << it->first << endl << it->second
               << endl;
      }
      cout<<endl;
      using stab::operator <<;
      cout <<"R0.........................................................R15 "<< endl;
      copy(registers.begin(), registers.end(),
            
            ostream_iterator<stab::types>(cout, " | "));
      cout<<endl;
      copy(symtab.begin(), symtab.end(),
            ostream_iterator<ScopeTable>(cout, "\n---------------\n"));

   }
   //! default constructor
   SymbolTable():free_reg_index(0) {
      for (size_t i = 0; i < NUM_OF_REGISTERS; ++i) {
         registers.push_back(stab::NO_TYPE);
      }
      newScope();
   }
   
   SymbolTable(const SymbolTable&) =delete; 
   void operator=(const SymbolTable& )=delete;

   ~SymbolTable() {
      symtab.clear();
   }
};

#endif
