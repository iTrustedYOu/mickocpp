     %skeleton "lalr1.cc" /* -*- C++ -*- */
     %require "2.7"
     %defines
     %define parser_class_name "mickocppxx_parser"


     %code requires {
     #include <string>
     #include <cstring>
     #include <iostream>
     #include <stack>
     #include <memory>
     using namespace std;
	 class mickocppxx_driver;
     }


     // The parsing context.
     %parse-param { mickocppxx_driver& driver }
     %lex-param   { mickocppxx_driver& driver }


     %locations
     %initial-action
     {
       // Initialize the initial location.
       @$.begin.filename = @$.end.filename = &driver.file;
     };


     %debug
     %error-verbose


     // Symbols.
     %union
     {
       int          ival;
       std::string *sval;
     };


     %code {
     # include "codegen.hh"
     # include "mickocpp-driver.hh"
     //# include "symbolTable.hh"
     
     SymbolTable s;
     std::string currentFunction;
     static int local_var_num=0;
     static int paramState=1;
     static size_t argumentCounter=0;
     
     static stack<size_t> prevArgCounter;
     static stack<std::string> currentFunctionCall;
     void check_main(mickocppxx_driver& driver, yy::mickocppxx_parser::location_type& yylloc);
     
     static stack<size_t> label_stack;
     static stack<size_t> false_label_stack;
     static size_t lab_num=0;
     static size_t false_lab_num=0;
     
     
     }


      %token END 0 "end of file"
      %token <ival>_TYPE
      %token _IF
      %token _ELSE
      %token _RETURN
      %token <sval> _ID
      %token <sval> _INT_NUMBER
      %token <sval> _UNSIGNED_NUMBER
      %token <sval> _CHAR_NUMBER
      %token _LOGICAL_OR
      %token _LOGICAL_AND
      %token _LPAREN
      %token _RPAREN
      %token _LBRACKET
      %token _RBRACKET
      %token _ASSIGN
      %token _SEMICOLON
      %token _COMMA
      %token _WHILE
      %token <ival> _AROP
      %token <ival> _MUOP
      %token <ival> _RELOP
      
      %type <sval> exp num_exp mul_exp  constant variable function_call
      %type <ival> type rel_exp log_exp and_exp
      
      %left _AROP
      %left _MUOP

   //AKO RAZUMEMO KASNIJE
      %printer    { yyoutput << *$$; } _ID
      %destructor { delete $$; } _ID
     
     %printer    { yyoutput << $$; } <ival>

     %%
     %start program;
     
program
  : variable_list function_list
  {  
     check_main(driver,yylloc);
     
  }
  | function_list{
  
     check_main(driver,yylloc);
  }
  ;

function_list
  : function
  | function_list function
  ;
  


function
  : type _ID 
  {
      
      currentFunction=*$2;
      paramState=-1;
      if(!s.tryToInsertID(currentFunction, (stab::types)$1, new globalAttribute(true)))
         error(yylloc,"Ups! Redeklaracija funkcije.");
      s.newScope();
      local_var_num=0;
      
      prepare_for_function(*$2);
       
  }
  
  _LPAREN parameters _RPAREN
  {
      paramState=1;
      local_var_num=0;
  }
   body
  {
      exit_function(*$2);
      s.clearScope();
  
  }
   
  ;
  
parameters
   :/*empty*/
   |parameter_list
   ;
parameter_list
   :variable
   {
      auto it=s.lookupID (currentFunction);
     
      it->second.attribute_ptr->addParamType( s.getType($1));
   }
   |parameter_list _COMMA variable
   {
      auto it=s.lookupID(currentFunction);
      
     
      it->second.attribute_ptr->addParamType( s.getType($3));
   }
   ;


type
  : _TYPE
  {
   $$=$1;
  }
  ;

body
  : _LBRACKET variable_list
  {
      variable_declaration(currentFunction, local_var_num);
      
  
  } statement_list _RBRACKET
  | _LBRACKET 
  {
   gen_str_lab(currentFunction, "_body");
  }
  
  statement_list _RBRACKET
  ;

variable_list
  : variable _SEMICOLON
  {
   cout<<"variable _SEMICOLON"<<endl;
  }
  
  | variable_list variable _SEMICOLON
  ;

variable
  : type _ID
      {
         
         if(s.getScope()==stab::GLOBAL_SCOPE){
            if(!s.tryToInsertID(*($2),(stab::types)$1, new globalAttribute{false}))
               error(yylloc,"Ups! Vec ste deklarisali promenljivu '"+*($2)+ "' na globalnom nivou");
         }else{
            local_var_num+=paramState;
            if(!s.tryToInsertID(*($2),(stab::types)$1, new variableAttribute(local_var_num)))
               error(yylloc,"Ups! Vec ste deklarisali promenljivu '"+*($2)+ "'!");               
         }
         $$=$2;
         //if(currentFunction=="f3") s.printSymtab();
      
      
      }
  ;

statement_list
  : /* empty */
  | statement_list statement
  ;

statement
  : compound_statement
  | assignment_statement
  | if_statement
  | return_statement
  | function_call_statement
  | while_statement
  ;

compound_statement
  : _LBRACKET
  {
      s.newScope();
      local_var_num=0;
      cout<<"compound statement"<<endl;
  
  }
  statement_list _RBRACKET
  {
     s.clearScope();
   }
   | _LBRACKET
   {
      s.newScope();
      local_var_num=0;
   
   }
   
    variable_list statement_list _RBRACKET
   {
      s.clearScope();
   }
  ;

assignment_statement
  : _ID _ASSIGN num_exp _SEMICOLON
   {
      auto it=s.lookupID(*$1);
      if(it==s.end()){
         error(yylloc, "Promenljiva '"+ *$1 +"'nije deklarisana");
      } else if(it->second.type!=s.getType($3)){
         error(yylloc, "Tip levog operanda ne odgovara tipu desnog operanda");
         cout<< "tip desnog je "<<s.getType($3)<<" tip levog je "<<it->second.type<<endl;
      } else {
         gen_mov(*$3, *$1,s);
      }
      
      
      
   
   }
  ;

num_exp
  : mul_exp
  {
      $$=$1;
  }
  | num_exp _AROP mul_exp
  {
      if(s.getType($1)!=s.getType($3)){
         error(yylloc,"Operandi nisu istog tipa");
      } else{
         $$=new std::string{ gen_arith($2, *$1, *$3,s) };
      }
  
  }
  ;

mul_exp
   :exp
   {
      $$=$1;
   }
   |mul_exp _MUOP exp
   {
      if(s.getType($1)!=s.getType($3)){
         error(yylloc,"Operandi nisu istog tipa! ");
      } else {
      
      $$=new std::string{ gen_arith($2, *$1, *$3,s) };
      }
      
   }
   ;
   

exp
  : constant
  {
      $$=$1;
  }
  | _ID
  {
      auto it=s.lookupID(*$1);
      if(it==s.end()){
         error(yylloc,"Promenljiva '"+*$1+"' nije deklarisana");
         
      }
      $$=$1;
  }
  | function_call
  {
  
  }
  | _LPAREN num_exp _RPAREN
  {
    $$=$2;
  }
  | _AROP exp
  {
      $$=$2;
  }
  ;

constant
  : _INT_NUMBER
  {
      //vraca iterator unordered_mape
      s.tryToInsertIntConstant(*$1);
      $$=$1;
      
  }
  | _CHAR_NUMBER 
  {
      //znaci mi cemo prosledjivati
      //ovo kao string ascii koda
      //to je ok, posto cemo tako odredjivati
      //u kojoj tabeli cemo gledati
      s.tryToInsertCharConstant(*$1);
      //ovako cemo prepoznavati char-ove
      $$=$1;
  }
  | _UNSIGNED_NUMBER
  {
      //promenicemo kasnije
      s.tryToInsertUnsignedConstant(*$1);
      $$=$1;
  }
  ;


function_call
  : _ID _LPAREN
  {
      
      auto it=s.lookupID(*$1, FUNCTION);
      if(it==s.end()){
         error(yylloc,"Funkcija '" + *($1) + "' nije definisiana");
         
      };
      
      currentFunctionCall.push(*$1);
      prevArgCounter.push(argumentCounter);
      argumentCounter=0;
      gen_function_call(*$1,s);
      
      
     
  }
  
   arguments _RPAREN
  {
    auto it=s.lookupID(*$1, FUNCTION);
    if(it!=s.end()) {
    
         if(argumentCounter!=it->second.attribute_ptr->numOfArgs()){
            error(yylloc,"Broj argumenata nije odgovarajuci za poziv funkcije '"+*($1)+"'");
         }
      
      }
      $$ =new std::string{ "%"+to_string(s.takeReg()) };
      //cout<<"tip povratne vrednosti funkcije "<<currentFunctionCall.top()<<" je"<<s.getType(&currentFunctionCall.top(), FUNCTION)<<endl;
      cout<<"zauzimamo registar "<<*$$<<" i stavljamo ga da je tipa "<<s.getType(&currentFunctionCall.top(), FUNCTION)<<endl;
      
      //s.setRegisterType(*$$, s.getType(&currentFunctionCall.top(), FUNCTION));
      string temp{"%13"};
      s.setRegisterType(temp, s.getType($1,FUNCTION) );
      gen_mov(temp,*$$, s);
      cout<< "izlazim iz poziva "<<currentFunctionCall.top()<<endl;
      currentFunctionCall.pop();
      
     
      
      
      argumentCounter=prevArgCounter.top();
      prevArgCounter.pop();
      
  }
  ;
  
function_call_statement
   : function_call _SEMICOLON
   {
      cout<<"statement: treba da otpustimo registar "<<*$1<<endl;
      release_register(*$1,s);
   }
   ;
arguments
   :/*empty*/
   | argument_list
   ;
   
argument_list
   :num_exp
   {
      auto it=s.lookupID(currentFunctionCall.top(), FUNCTION);
      cout<<"trenutno smo u pozivu "<<currentFunctionCall.top()<<" funkcije "<<endl;
      if(it!=s.end()){
         if(it->second.attribute_ptr->getParamType(argumentCounter)!=s.getType($1)){
            error(yylloc,"Argument "+to_string(argumentCounter)+ "koji je tipa "+ to_string(s.getType($1))+
               " se ne poklapa sa parametrom " + to_string(it->second.attribute_ptr->getParamType(argumentCounter))+ " funkcije '"+ currentFunctionCall.top() +"',");
         }
      } else {
         error(yylloc, "Prosledjujete argumente nepostojacoj funkciji");
      }
         ++argumentCounter;
      release_register(*$1,s);
      
   }
   | argument_list _COMMA num_exp
   {
     
     auto it=s.lookupID(currentFunctionCall.top(), FUNCTION);
      if(it!=s.end()){
         if(it->second.attribute_ptr->getParamType(argumentCounter)!=s.getType($3)){
            error(yylloc,"Argument "+to_string(argumentCounter)+
               " se ne poklapa sa parametrom" + to_string(it->second.attribute_ptr->getParamType(argumentCounter))+" funkcije '"+ currentFunctionCall.top() +"'");
         }
      } else {
         error(yylloc, "Prosledjujete argumente nepostojacoj funkciji");
         
      }
         ++argumentCounter;
         release_register(*$3,s);
   
   
   }
   ;
   



if_statement
  : if_part
  {
      gen_num_lab("exit", label_stack.top());
      label_stack.pop();
      false_label_stack.pop();
  }
  | if_part _ELSE  statement
  {
      gen_num_lab("exit", label_stack.top());
      label_stack.pop();
	  false_label_stack.pop();
  }
  ;

if_part
  : _IF _LPAREN  
  {
   
    gen_num_lab("if",lab_num);
    
  }  rel_exp 
  {
      
      gen_false_jmp($4, false_lab_num);
      gen_num_lab("true", lab_num);
      label_stack.push(lab_num);
      lab_num++;
      false_label_stack.push(false_lab_num);
      false_lab_num++;
      
  } 
  _RPAREN statement
  {
      
        gen_exit_if_part(label_stack.top());
        
        gen_num_lab("false", false_label_stack.top());
        lab_num=false_lab_num;

  
  }
  ;
  
log_exp
   : and_exp
   {
		$$ = $1;
   }
   | log_exp _LOGICAL_OR
   {
		gen_true_jmp($1, lab_num);
		gen_num_lab("false", false_lab_num);
   }
   and_exp
   {
		$$=$4;
   }
   ;
   
and_exp
   : rel_exp
   {
		$$=$1;
		++false_lab_num;
   }
   | and_exp _LOGICAL_AND 
   {
		gen_false_jmp($1, false_lab_num);
   }
   rel_exp
   {
		$$ = $4;
   }
   
   ;

rel_exp
  : num_exp _RELOP num_exp
  {
      if(s.getType($1)!=s.getType($3)){
         error(yylloc,"Nekompatibilni tipovi u relacionom operatoru");
      }else{
         gen_cmp(*$1,*$3,s);
      }
      $$=$2;
  }
  ;
  
while_statement
   : _WHILE 
   {
		lab_num++;
		gen_num_lab("while", lab_num);
		label_stack.push(lab_num);
   }
	 _LPAREN log_exp
	 {
		gen_false_jmp($4, false_lab_num);
		false_label_stack.push(false_lab_num);
		gen_num_lab("true", lab_num);
	 }
	 _RPAREN statement
	{
		gen_jmp ("while", label_stack.top());
		gen_num_lab("false", false_label_stack.top());
		gen_num_lab("exit", label_stack.top());
		label_stack.pop();
		false_label_stack.pop();
      lab_num=false_lab_num;
	}
   ;

return_statement
  : _RETURN num_exp _SEMICOLON
  {
      auto it=s.lookupID(currentFunction,FUNCTION);
      cout<<" trenutna funkcija je "<<currentFunction;
      if(it->second.type!=s.getType($2)){
      //da se ispisu tipovi
         error(yylloc,"Neodgovarajuci tip povratne vrednosti za funkciju " + currentFunction);
      } else{
         std::string func{FUNCTION_REGISTER};
         gen_mov(*$2, func,s);
      }
  
  }
  ;


     %%
     

void
yy::mickocppxx_parser::error (const yy::mickocppxx_parser::location_type& l,
                         const std::string& m)
{
 driver.error (l, m);
}





void check_main( mickocppxx_driver& driver,yy::location& yylloc){
   auto it=s.lookupID("main",FUNCTION);
   if(it==s.end()){
      driver.error(yylloc,"Nedefinisana referenca na main");
   }else if(it->second.type!=stab::INT_TYPE){
      driver.error(yylloc,"Pogresan tip za main- potreban int");
   }else if(it->second.attribute_ptr->numOfArgs()!=0){
      driver.error(yylloc, "main ne treba da ima parametre");
   }

}


int
main (int argc, char *argv[])
{
   std::cout<<std::endl<<"START"<<std::endl;
   open_output();
   
   mickocppxx_driver driver;
   for (int i = 1; i < argc; ++i){
     if (!driver.parse (argv[i])){
      cout<<"error count "<<driver.get_error_count()<<endl;
      
     } else {
      std::cout << "nesto se desilo" << std::endl;
     }
     
   } 
   
   close_output();
   std::cout<<std::endl<<"END"<<std::endl;
    
}

