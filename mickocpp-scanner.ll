


%{ /* -*- C++ -*- */
# include <cstdlib>
# include <cerrno>
# include <climits>
# include <string>
# include "defs.h"
# include "mickocpp-driver.hh"
# include "mickocpp-parser.tab.hh"

/* Work around an incompatibility in flex (at least versions
  2.5.31 through 2.5.33): it generates code that does
  not conform to C89.  See Debian bug 333231
  <http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=333231>.  */
# undef yywrap
# define yywrap() 1

/* By default yylex returns int, we use token_type.
  Unfortunately yyterminate by default returns 0, which is
  not of token_type.  */
#define yyterminate() return token::END
%}


%option noyywrap nounput batch debug


id    [a-zA-Z][a-zA-Z0-9]*
blank [ \t]


%{
# define YY_USER_ACTION  yylloc->columns (yyleng);
%}

%%

%{
 yylloc->step ();
%}


{blank}+   yylloc->step ();
[\n]+      yylloc->lines (yyleng); yylloc->step ();


%{
 typedef yy::mickocppxx_parser::token token;
%}




   "int"                { yylval->ival=INT_TYPE; return token::_TYPE; }
   "char"               { yylval->ival=CHAR_TYPE; return token::_TYPE;}
   "unsigned"           { yylval->ival=UNSIGNED_TYPE; return token::_TYPE;}
   "if"                 { return token::_IF; }
   "else"               { return token::_ELSE; }
   "while"              { return token::_WHILE; }
   "return"             { return token::_RETURN; }

   "("                  { return token::_LPAREN; }
   ")"                  { return token::_RPAREN; }
   "{"                  { return token::_LBRACKET; }
   "}"                  { return token::_RBRACKET; }
   ";"                  { return token::_SEMICOLON; }
   ","                  { return token::_COMMA    ; }
   "="                  { return token::_ASSIGN; }
   "+"                  { yylval->ival = ADD; return token::_AROP; }
   "-"                  { yylval->ival = SUB; return token::_AROP; }
   "*"                  { yylval->ival  = MUL; return token::_MUOP; }
   "/"                  { yylval->ival  = DIV; return token::_MUOP; }
   "=="                 { yylval->ival = EQ; return token::_RELOP; }
   "!="                 { yylval->ival = NE; return token::_RELOP; }
   ">="                 { yylval->ival = GE; return token::_RELOP; }
   "<="                 { yylval->ival = LE; return token::_RELOP; }
   "<"                  { yylval->ival = LT; return token::_RELOP; }
   ">"                  { yylval->ival = GT; return token::_RELOP; }
   "||"                 {  return token::_LOGICAL_OR; }
   "&&"                 {  return token::_LOGICAL_AND; }

   {id}                 { yylval->sval = new std::string (yytext);
                           return token::_ID; }
                          
    [0-9]{1,10}     { yylval->sval = new std::string (yytext); 
                           return token::_INT_NUMBER;}
    
     \+?[0-9]{1,10}[uU]   { yylval->sval = new std::string (yytext);
                              return token::_UNSIGNED_NUMBER;}
                              
    \'[A-Za-z]\'       {   int i=yytext[1];
                           yylval->sval=new std::string (to_string(i) + "c");
                           cout<<"char konstanta prepoznata je"<<*yylval->sval<<endl;
                           return token::_CHAR_NUMBER;}

   \/\/.*               { /* skip */ }
   .                    driver.error (*yylloc, "Leksicka greska kod karaktera: '"+ std::string(yytext)+"'");

%%

void
mickocppxx_driver::scan_begin ()
{
  yy_flex_debug = trace_scanning;
  if (file.empty () || file == "-")
    yyin = stdin;
  else if (!(yyin = fopen (file.c_str (), "r")))
    {
      error ("cannot open " + file + ": " + strerror(errno));
      exit (EXIT_FAILURE);
    }
}



void
mickocppxx_driver::scan_end ()
{
  fclose (yyin);
}

