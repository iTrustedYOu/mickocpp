#include <iostream>
#include <vector>
#include <cassert>
#include <unordered_map>
#include <iterator>
#include <climits>
#include <sstream>
#include <stdexcept>
#include <memory>
#include <typeinfo>

#include "symbolTable.hh"
using namespace std;
//MORAMO SREDITI OVE MEMORY LEAKOVE
//#define DEBUG


ostream& operator<<(ostream& os, const Attribute& attr) {

   if (typeid(attr) == typeid(globalAttribute)) {
      os << "global attribute: ";
      if (attr.getAttribute() == 0)
         return (os << "nema");
      for (size_t i = 0; i < attr.numOfArgs(); ++i) {
         os << attr.getParamType(i) << " ";

      }
   } else if (typeid(attr) == typeid(variableAttribute)) {
      os << "local attribute: ";
      os << attr.getAttribute() << endl;
   }
   return os;
}



ostream& operator<<(ostream& os, const symbolValue& symVal) {
   os << "--------------------"<<endl;
   os <<"name: "<< symVal.name << endl << "type: "<<symVal.type << endl
        << "attribute: "<< *symVal.attribute_ptr << endl;
   os << "--------------------"<<endl;
   return os;
}

namespace stab{
   
ostream& operator <<(ostream& os, types rhs){
   static string enum_string[]={"NO_TYPE","INT_TYPE", "UNSIGNED_TYPE", "CHAR_TYPE"};
   os<<enum_string[rhs];
   return os;
}
}

ostream& operator<<(ostream& out, const ScopeTable& sTable) {
   for (auto it = sTable.tableMap.begin(); it != sTable.tableMap.end(); ++it) {
      out << it->second << endl;
   }
   return out;
}


void testing();
void atributTestovi();

//~ int main() {
   //~ testing();
   //~ atributTestovi();
//~ 
   //~ //testovi za atribut funkcija
//~ 
   //~ cout << "Svi testovi prosli- vi ste odlicni!" << endl;
//~ 
//~ }

void atributTestovi() {

   globalAttribute gAt { true };
   Attribute& ref = gAt;

   ref.addParamType(stab::NO_TYPE);
   ref.addParamType(stab::INT_TYPE);
   ref.addParamType(stab::UNSIGNED_TYPE);
   ref.addParamType(stab::CHAR_TYPE);
   assert(ref.getParamType(0) == stab::NO_TYPE);
   assert(ref.getParamType(1) == stab::INT_TYPE);
   assert(ref.getParamType(2) == stab::UNSIGNED_TYPE);
   assert(ref.getParamType(3) == stab::CHAR_TYPE);
   cout << ref << endl;
   assert(ref.numOfArgs() == 4);
   cout << ref.getParamType(0) << endl;

   variableAttribute vAt;
   Attribute& ref2 = vAt;
   cout << ref2 << endl;
   assert(ref2.getAttribute() == 0);
   ref2.setAttribute(5);
   cout << ref2 << endl;
   assert(ref2.getAttribute() == 5);
   ref2.setAttribute(-4);
   assert(ref2.getAttribute() == -4);

   //testovi za atribut

}

void testing() {

   ScopeTable testST;
   symbolValue testSV;
   cout << testSV << endl;
   symbolValue testSV1 { "ime1", stab::INT_TYPE, new variableAttribute { 1 } };
   symbolValue testSV2 { "ime1", stab::UNSIGNED_TYPE, new variableAttribute { 1111 } };
   symbolValue testSV3 { "ime2", stab::CHAR_TYPE, new variableAttribute { 1111 } };
   symbolValue testSV4 { "ime2", stab::INT_TYPE, new variableAttribute { 1 } };
   assert(testST.size() == 0);
   assert(testST.find("ime") == testST.end());
   assert(testST.insertSymbol(testSV) == true);
   assert(testST.find("ime") != testST.end());
   assert(testST.size() == 1);
#ifdef DEBUG
   cout << "Ubacen prazan " << endl;
   cout << "Sad je sadrzaj:\n " << testST << endl;
#endif
   assert(testST.insertSymbol(testSV) == false);
#ifdef DEBUG
   cout << "Sad je sadrzaj isti: \n" << testST << endl;
#endif
   assert(testST.find(testSV1.name) == testST.end());
   assert(testST.insertSymbol(testSV1) == true);
   assert(testST.find(testSV1.name) != testST.end());

   assert(testST.size() == 2);
#ifdef DEBUG
   cout << "Sad je sadrzaj: \n" << testST << endl;
#endif
   assert(testST.insertSymbol(testSV1) == false);
#ifdef DEBUG
   cout << "Sad je sadrzaj isti:\n " << testST << endl;
#endif

   assert(testST.insertSymbol(testSV1) == false);
#ifdef DEBUG
   cout << "Sad je sadrzaj isti: \n" << testST << endl;
#endif

   assert(testST.insertSymbol(testSV3) == true);
   assert(testST.size() == 3);
#ifdef DEBUG
   cout << "Sad je sadrzaj:\n " << testST << endl;
#endif
   assert(testST.insertSymbol(testSV3) == false);
#ifdef DEBUG
   cout << "Sad je sadrzaj isti: \n" << testST << endl;
#endif

   assert(testST.insertSymbol(testSV4) == false);

   //testiranje symbolTable
   SymbolTable s;
#ifdef DEBUG
   s.printSymtab();
#endif
   s.newScope();
   std::string nesto="nesto";
   std::string *nestoPtr=&nesto;
   assert(s.tryToInsertID(nesto, stab::INT_TYPE, new globalAttribute(false)) == true);
   assert(s.getType(nestoPtr)==stab::INT_TYPE);
   assert(s.tryToInsertID("nesto", stab::INT_TYPE, new globalAttribute(false)) == false);
   s.newScope();
   assert(s.tryToInsertID("nesto", stab::CHAR_TYPE, new globalAttribute(false)) == true);
   assert(s.tryToInsertID("nesto", stab::CHAR_TYPE, new globalAttribute(false)) == false);
   assert(s.getType(nestoPtr)==stab::CHAR_TYPE);
   cout << "pre lookupid" << endl;
   assert(s.lookupID("nesto") != s.end());
   s.clearScope();
   assert(s.lookupID("nesto") != s.end());
   s.clearScope();
   assert(s.lookupID("nesto") == s.end());
   std::string intNum="1234";
   std::string charNum="99c";
   std::string unsignedNum="44";
   std::string negIntNum="-4333";
   s.tryToInsertIntConstant(intNum);
   s.tryToInsertIntConstant(charNum);
   s.tryToInsertUnsignedConstant(unsignedNum);
   s.tryToInsertIntConstant(negIntNum);
   assert(s.getType(&intNum)==stab::INT_TYPE);
   //assert(s.getType(&cCharNum)==stab::CHAR_TYPE);
   assert(s.getType(&unsignedNum)==stab::UNSIGNED_TYPE);
   assert(s.getType(&negIntNum)==stab::INT_TYPE);
   

   s.tryToInsertID("ime funkcije", stab::INT_TYPE,
         new globalAttribute { true });
   auto it = s.lookupID("ime funkcije", FUNCTION);
   assert(it != s.end());
   it->second.attribute_ptr->addParamType(stab::INT_TYPE);
   it->second.attribute_ptr->addParamType(stab::CHAR_TYPE);
   it->second.attribute_ptr->addParamType(stab::UNSIGNED_TYPE);
   cout << "kad udjemo ovde?" << endl;
   s.printSymtab();

   cout << it->second.attribute_ptr->getParamType(0) << endl;
   cout << it->second.attribute_ptr->getParamType(1) << endl;
   cout << it->second.attribute_ptr->getParamType(2) << endl;
   it->second.attribute_ptr->addParamType(stab::CHAR_TYPE);
   it->second.attribute_ptr->addParamType(stab::UNSIGNED_TYPE);
   assert(s.getScope()==stab::GLOBAL_SCOPE);
   s.newScope();
   assert(s.getScope()==stab::LOCAL_SCOPE);
   s.newScope();
   assert(s.getScope()==stab::LOCAL_SCOPE);
   s.newScope();
   assert(s.getScope()==stab::LOCAL_SCOPE);
   s.newScope();
   assert(s.getScope()==stab::LOCAL_SCOPE);
   s.clearScope();
   s.clearScope();
   s.clearScope();
   s.clearScope();
   assert(s.getScope()==stab::GLOBAL_SCOPE);
   
   
   

}

//~ void testKonverzija() {
   //~ SymbolTable s;
//~ 
   //~ //testiranje konverzija
   //~ s.tryToInsertConstant("4294967296", stab::UNSIGNED_TYPE);
   //~ s.tryToInsertConstant("-1", stab::UNSIGNED_TYPE);
   //~ s.tryToInsertConstant("45633", stab::UNSIGNED_TYPE);
   //~ s.tryToInsertConstant("-455", stab::INT_TYPE);
   //~ cout << "dobro" << endl;
   //~ s.tryToInsertConstant("2147483648", stab::INT_TYPE);
   //~ s.tryToInsertConstant("2147483647", stab::INT_TYPE);
   //~ s.tryToInsertConstant("    -4   5s   s    ss5", stab::INT_TYPE);
   //~ //testiranje staticke lokalne promenljive
   //~ cout << isConstant() << endl;
   //~ isConstant() = false;
   //~ cout << isConstant() << endl;
   //~ s.printSymtab();
//~ 
//~ }


