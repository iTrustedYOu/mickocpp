/* mC generator koda na HAJ */

%{
#include <stdio.h>
#include <stdlib.h>
extern int yylineno;
%}

%union {
  int i;
  char *s;
}



%token <i>_TYPE
%token _IF
%token _ELSE
%token _RETURN
%token <s> _ID
%token <s> _INT_NUMBER
%token <s> _UNSIGNED_NUMBER
%token _LOGICAL_OR
%token _LOGICAL_AND
%token _LPAREN
%token _RPAREN
%token _LBRACKET
%token _RBRACKET
%token _ASSIGN
%token _SEMICOLON
%token _COMMA
%token _WHILE
%token <i> _AROP
%token <i> _MUOP
%token <i> _RELOP

%%

program
  : variable_list function_list
  {   //check if main exists
  }
  | function_list{
      //check if main exists
  }
  ;

function_list
  : function
  | function_list function
  ;
  


function
  : type _ID _LPAREN parameters _RPAREN body
  ;
  
parameters
   :/*empty*/
   |parameter_list
   ;
parameter_list
   :variable
   |parameter_list _COMMA variable
   ;


type
  : _TYPE{
   //prosledjujemo brojeve
  }
  ;

body
  : _LBRACKET variable_list statement_list _RBRACKET

  | _LBRACKET statement_list _RBRACKET
  ;

variable_list
  : variable _SEMICOLON
  | variable_list variable _SEMICOLON
  ;

variable
  : type _ID
  ;

statement_list
  : /* empty */
  | statement_list statement
  ;

statement
  : compound_statement
  | assignment_statement
  | if_statement
  | return_statement
  | function_call_statement
  | while_statement
  ;

compound_statement
  : _LBRACKET statement_list _RBRACKET
  ;

assignment_statement
  : _ID _ASSIGN num_exp _SEMICOLON
  ;

num_exp
  : mul_exp
  | num_exp _AROP mul_exp
  ;

mul_exp
   :exp
   |mul_exp _MUOP exp
   ;
   

exp
  : constant
  | _ID
  | function_call
  | _LPAREN num_exp _RPAREN
  | _AROP exp
  ;

constant
  : _INT_NUMBER
  | _UNSIGNED_NUMBER
  //| _CHAR_CONSTANT mozda ne dozvoljavamo char constante????
  ;


function_call
  : _ID _LPAREN arguments _RPAREN
  ;
  
function_call_statement
   : function_call _SEMICOLON
   ;
arguments
   :/*empty*/
   | argument_list
   ;
   
argument_list
   :num_exp
   | argument_list _COMMA num_exp
   ;
   



if_statement
  : if_part
  | if_part _ELSE  statement
  ;

if_part
  : _IF _LPAREN    rel_exp  _RPAREN statement
  ;
  
log_exp
   : and_exp
   | log_exp _LOGICAL_OR and_exp
   ;
and_exp
   : rel_exp
   | and_exp _LOGICAL_AND rel_exp
   ;

rel_exp
  : num_exp _RELOP num_exp
  ;
  
while_statement
   : _WHILE _LPAREN log_exp _RPAREN statement
   ;

return_statement
  : _RETURN num_exp _SEMICOLON
  ;


%%


/*int yyerror(string s) {
  //nista
}

void warning(string s) {
}
*/

int main() {
  printf("\nSTART\n");
  SymbolTable
 // init_stack(&label_stack);
  //output = fopen("output.asm", "w+");

  yyparse();

  //fclose(output);
  printf("\nSTOP\n");
  
  //if(error_count)
   // remove("output.asm");
  return EXIT_SUCCESS;
}


