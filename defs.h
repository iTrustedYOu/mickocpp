#ifndef DEFS_H
#define DEFS_H


#define SYMBOL_TABLE_LENGTH   64
#define CHAR_BUFFER_LENGTH    128
#define NO_ATTRIBUTE          -1
#define LAST_WORKING_REGISTER 12
#define FUNCTION_REGISTER     ("%13")

extern char char_buffer[CHAR_BUFFER_LENGTH];

//tipovi podataka
//~ enum types { NO_TYPE, INT_TYPE, VOID_TYPE };
//OVO NE SME BITI TAKO DUPLICIRANO AAAAAAAAAAAAAAA
enum types {NO_TYPE=0,INT_TYPE, UNSIGNED_TYPE,CHAR_TYPE};

// vrste simbola (moze ih biti maksimalno 32)
//~ enum kinds { NO_KIND = 0x1, REGISTER = 0x2, CONSTANT = 0x4, 
   //~ FUNCTION = 0x8, LOCAL_VAR = 0x10 };
   
enum kinds{ FUNCTION= 0x1, VAR= 0x2};

//konstante arithmetickih operatora
enum arops { ADD=0, SUB=1 };
enum muops { MUL=2, DIV=3 };

//konstante relacionih operatora
enum relops { EQ=0, NE=1, LE=2, GE=3, GT=4, LT=5 };

#endif
