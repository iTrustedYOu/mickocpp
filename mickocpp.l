/* Skener za mikroC */
%option noyywrap yylineno

%{
   #include <string.h>
   #include "mickocpp.tab.h"
   #include "defs.h"

   extern YYSTYPE yylval;
%}

%%

[ \t\n]+             { /* skip */     }

"int"                { yylval.i=INT_TYPE; return _TYPE; }
"char"               { yylval.i=CHAR_TYPE; return _TYPE;}
"unsigned"           { yylval.i=UNSIGNED_TYPE; return _TYPE;}
"if"                 { return _IF; }
"else"               { return _ELSE; }
"while"              { return _WHILE; }
"return"             { return _RETURN; }

"("                  { return _LPAREN; }
")"                  { return _RPAREN; }
"{"                  { return _LBRACKET; }
"}"                  { return _RBRACKET; }
";"                  { return _SEMICOLON; }
"="                  { return _ASSIGN; }
"+"                  { yylval.i = ADD; return _AROP; }
"-"                  { yylval.i = SUB; return _AROP; }
"=="                 { yylval.i = EQ; return _RELOP; }
"!="                 { yylval.i = NE; return _RELOP; }

[a-zA-Z][a-zA-Z0-9]* { yylval.s = strdup(yytext); 
                       return _ID; }
[+-]?[0-9]{1,10}     { yylval.s = strdup(yytext); 
                       return _INT_NUMBER;}

\/\/.*               { /* skip */ }
.                    { printf("line %d: LEXICAL ERROR on char"
                              "%c\n", yylineno, *yytext);}

%%

