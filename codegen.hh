#ifndef CODEGEN_HH
#define CODEGEN_HH

#include <fstream>
#include <array>
#include "symbolTable.hh"

#define TODO //.
/**
 * 
 * 
 */
void gen_function_call(string& ime, SymbolTable& s);
void gen_exit_if_part(size_t lab_num);
void gen_false_jmp(int relop, size_t labnum);
void gen_true_jmp(int relop, size_t labnum);
void gen_jmp (const string& str, int labnum);
void gen_mov(std::string& input_string, std::string& output_string, SymbolTable& s);
void open_output();
void close_output();
std::string indent(bool tabs);
void exit_function(const std::string& functionName);
void gen_str_lab(std::string str1, std::string str2) ;
void gen_num_lab(const std::string& str, int num);
std::string print_symbol(std::string ime, SymbolTable& s);
void prepare_for_function(const std::string& functionName);
void release_register(const std::string& maybeRegister,SymbolTable& s);
void variable_declaration(const std::string& functionName, int var_num);
std::string gen_arith(int statement, std::string& operand1, std::string& operand2, SymbolTable& s);
void gen_cmp(std::string& operand1, std::string& operand2, SymbolTable& s);

#endif

