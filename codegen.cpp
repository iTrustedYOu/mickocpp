
#include "codegen.hh"

static ofstream fout;

using namespace std;
 
void gen_mov( std::string& input_string,  string& output_string, SymbolTable& s) {
   
   release_register(input_string,s);
   fout<<indent(true)<<"MOV \t";
   fout<<print_symbol(input_string,s);
   fout<<","<<print_symbol(output_string,s);

   //~ //ako smestas u registar, prenesi tip 
   if(s.isRegister(output_string)){
      cout<<output_string<<" jeste registar" <<endl;
     s.setRegisterType(output_string, s.getType(&input_string));
   }
}


void open_output(){
   fout.open("output.asm");
}

void close_output(){
   fout.close();
}


string indent(bool tabs) {
   string s ("\n");
   if(tabs)
     s+="\t\t\t";
   return s;
}

void gen_str_lab(string str1, string str2) {
   fout<<indent(false);
   fout<<"@"<<str1<<str2<<":";
}

void exit_function(const string& functionName){
        gen_str_lab(functionName,"_exit");
        fout<<indent(true)<<"MOV"<<"\t%14,%15";
        fout<<indent(true)<<"POP"<<"\t%14";
        fout<<indent(true)<<"RET";
}

void gen_num_lab(const string& str, int num) {
   fout<<indent(false);
   fout<<"@"<<str<<num<<":";
}
void gen_false_jmp(int relop, size_t labnum){
   const static std::array<string,6> opposite_jumps { {"JNE", "JE", "JGTS", "JLTS", "JLES", "JGES" }};        
   fout<<indent(true)<<opposite_jumps.at(relop)<<"\t";
   fout<<"@false"<<labnum;
   //gen_num_lab("true", labnum);
}

void gen_true_jmp(int relop, size_t labnum) {
	const static std::array<string,6> real_jumps { {"JE", "JNE", "JLES", "JGES", "JGTS", "JLTS" }};
	fout<<indent(true)<<real_jumps.at(relop)<<"\t";
	fout<<"@true"<<labnum;
}

void gen_exit_if_part(size_t lab_num){
   fout<<indent(true)<<"JMP\t"<<"@exit"<<lab_num;
   
}

void gen_jmp (const string& str, int labnum) {
	fout<<indent(true)<<"JMP\t"<<"@"<<str<<labnum;
}
//
// SYMBOL
//ovde mozemo prosledjivati mozda bolje
//odmah atribut. ovo trazenje malo puno traje
string print_symbol(std::string ime, SymbolTable& s) {
   int attribute;
   string printed;
      if(s.isConstant(ime)){
         //kako treba da se ispisuju char konstante??
         printed="$"+ ime.substr(0,ime.size()-(int)(ime.at(ime.size()-1)=='c'));
      } else if(s.isRegister(ime)){
         printed+=ime;
      } else {
         //hoce se ovde printati funkcije ili ne??
         auto it=s.lookupID(ime);
         attribute=it->second.attribute_ptr->getAttribute();
         if(attribute!=0){
            attribute=-attribute;
            printed+=to_string(4*attribute) +"(%14)";
         } else {
            printed+=ime;
         }
     }
      return printed;
}

void prepare_for_function(const string& functionName){
       fout<<endl<<functionName<<":";
       fout<<indent(true)<<"PUSH"<<"\t%14";
       fout<<indent(true)<<"MOV"<<"\t%15,%14";
}
/**
 * 
 * @todo skloniti ovaj 13
 */

void release_register(const string& maybeRegister,SymbolTable& s){
   if(maybeRegister[0]=='%' && stoul(maybeRegister.substr(1))< 13){
         cout<<"otpustamo registar "<<maybeRegister<<endl;
         s.freeReg();
   }
   
}
void variable_declaration(const string& functionName, int var_num){
      fout<<indent(true)<<"SUBU\t%15,$"<<4*var_num<<",%15";
      gen_str_lab(functionName, "_body");
}

string gen_arith(int statement, string& operand1, string& operand2, SymbolTable& s) {
   static array<string,4> operators { {"ADDS", "SUBS", "MULS", "DIVS"} };
   cout<<operators.at(0)<<operators.at(1)<<endl;
   string output_register;
   size_t output_index;
   release_register(operand1, s);
   release_register(operand2, s);
   cout<<"posle release_register"<<endl;
   output_index=s.takeReg();
   cout<<"posle take_register"<<endl;
   output_register="%" + to_string(output_index);
   cout<<"output_register= "<<output_register<<endl;
   fout<<indent(true)<<operators.at(statement)<<"\t";
   fout<<print_symbol(operand1,s)<<","<<print_symbol(operand2,s);
   fout<<","<<print_symbol(output_register,s);
   cout<<"posle print symbol"<<endl;
   //tip izraza = tip jednog od operanada (recimo prvog)
   s.setRegisterType(output_index, s.getType(&operand1));
   cout<<"posle setRegType.."<<endl;
   return output_register;
}


void gen_cmp(string& operand1, string& operand2, SymbolTable& s) {
   release_register(operand1,s);
   release_register(operand2,s);
   fout<<indent(true)<<"CMPS \t"<<print_symbol(operand1,s)<<",";
   fout<<print_symbol(operand2,s);
}

void gen_function_call(string& ime,SymbolTable& s) {
   fout<<indent(true)<<"CALL\t"<<ime;
}


//~ int main(){
   //~ open_output();
   //~ //cout<<print_symbol()<<endl;
   //~ cout<<indent(true)<<"POP"<<endl;
   //~ close_output();
   //~ 
//~ }
