#ifndef MICKOCPPXX_DRIVER_HH
     # define MICKOCPPXX_DRIVER_HH
     # include <string>
     # include <map>
     # include "mickocpp-parser.tab.hh"
     
     // Tell Flex the lexer's prototype ...
     # define YY_DECL                                        \
       yy::mickocppxx_parser::token_type                         \
       yylex (yy::mickocppxx_parser::semantic_type* yylval,      \
              yy::mickocppxx_parser::location_type* yylloc,      \
              mickocppxx_driver& driver)
     // ... and declare it for the parser's sake.
     YY_DECL;
     
     
     
     
     class mickocppxx_driver
     {
     public:
       mickocppxx_driver ();
       virtual ~mickocppxx_driver ();
     
       // Handling the scanner.
       void scan_begin ();
       void scan_end ();
       bool trace_scanning;
       size_t error_count;


       // Run the parser.  Return 0 on success.
       int parse (const std::string& f);
       size_t get_error_count()const{
            return error_count;
       }
       std::string file;
       bool trace_parsing;


       // Error handling.
       void error (const yy::location& l, const std::string& m);
       void error (const std::string& m);
     };
     
     
#endif // ! MICKOCPPXX_DRIVER_HH

     
     
