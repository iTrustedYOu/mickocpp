#include "mickocpp-driver.hh"
#include "mickocpp-parser.tab.hh"
     
     mickocppxx_driver::mickocppxx_driver ()
       : trace_scanning (false), error_count(0), trace_parsing (false)
     {
     }
     
     mickocppxx_driver::~mickocppxx_driver ()
     {
     }
     
     int
     mickocppxx_driver::parse (const std::string &f)
     {
       file = f;
       scan_begin ();
       //sta ovo znaci??
       yy::mickocppxx_parser parser(*this);
       parser.set_debug_level (trace_parsing);
       int res = parser.parse();
       scan_end();
       return res;
     }
     
     void
     mickocppxx_driver::error (const yy::location& l, const std::string& m)
     {
       std::cerr << l << ": " << m << std::endl;
       ++error_count;
     }
     
     void
     mickocppxx_driver::error (const std::string& m)
     {
       std::cerr << m << std::endl;
       ++error_count;
     }
